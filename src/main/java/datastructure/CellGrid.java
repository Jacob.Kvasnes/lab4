package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    ArrayList<ArrayList <CellState> > grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.grid = new ArrayList<ArrayList <CellState> >();
        for (int row = 0; row<rows; row++){
            ArrayList<CellState> listOfRow = new ArrayList<CellState>();
            for (int col = 0; col<cols; col++){
                listOfRow.add(initialState);
            }
            grid.add(listOfRow);
        }
        System.out.println(grid);
        
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) throws IndexOutOfBoundsException{
        grid.get(row).set(column,element);
    }

    @Override
    public CellState get(int row, int column) throws IndexOutOfBoundsException{
        return grid.get(row).get(column);
    }

    @Override
    public IGrid copy() {
        CellGrid copyGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int row = 0; row<this.rows;row++){
            for (int col = 0; col<this.cols;col++){
                copyGrid.set(row, col,this.get(row, col));
            }
        }
        return copyGrid;
    }

    
}
